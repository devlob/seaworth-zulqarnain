<!---JS---->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script> -->
<script src="http://code.jquery.com/jquery-3.3.1.js"></script>
<!-- <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js "></script> -->
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js "></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js "></script>
<!--Custom JS-->
<script src="./assets/js/custom.js "></script>
<script src="./assets/js/wow.min.js"></script>
<script>
    new WOW().init();
</script>
<!--Footer Section-->
<section>
    <div class=" pt-3  footer">
        <div class="row  text-center">
            <div class="col-sm-6 col-md-3 col-lg-3  about-company">
                <h4 class="mt-lg-0 mt-sm-3">Information</h4>
                <ul class="m-0 p-0 list-style " style="list-style: none;">
                    <li> <a href="index.html">Home </a></li>
                    <li> <a href="about.html">About Us</a></li>
                    <li> <a href="#">Products</a></li>
                    <li> <a href="#">Size Chart</a></li>
                    <li> <a href="#">SiteMap</a></li>
                    <li> <a href="contact.html">Contact Us</a></li>
                </ul>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-3  categories">
                <h4 class="mt-lg-0 mt-sm-3">Categories</h4>
                <ul class="m-0 p-0 list-style" style="list-style: none;">
                    <li><a href="#">Motorbike Apparel</a></li>
                    <li><a href="#">Tactical Product</a></li>
                    <li><a href="#">Etiam vitae mauris</a></li>
                    <li><a href="#">Sports wear</a></li>
                </ul>
            </div>
            <div class=" col-sm-6 col-md-3 col-lg-3  feedback">
                <h4 class="mt-lg-0 mt-sm-3">Your Feedback</h4>
                <p>Give us your feedback to help us improve your experience in browsing our site.Thank You!</p>
                <a href="#" class="btn btn-index  " type="button">Your Feedback here</a>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-3 contact-us">
                <h4 class="mt-lg-0 mt-sm-3">Contact us</h4>
                <p class="mb-0"><i class="fa fa-phone mr-3"></i>+92 523520151</p>
                <p class="mb-0"><i class="fa fa-shopping-bag mr-3"></i>+92 523613311</p>
                <p><i class="fa fa-envelope-o mr-3"></i>sales@donor.pk</p>
                <p class="mb-0 "><i class="fa fa-map-marker mr-3 "></i>Cheema Street,New Hamza House Sialkot Pakistan</p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-sm-6 col-md-6 mt-lg-0 ">
                <span class="mr-1 ml-3" style="font-size:22px; color: #fff;">Write us an email</span>
                <span class="text-light">Signup to get the latest stories and free returns!</span>
            </div>
            <div class="col-sm-6 col-md-6  mt-lg-0">
                <div class="row">
                    <div class="col-sm-6 col-md-6 ">
                        <input type="text" class="form-control" placeholder="Enter Your Email!">
                    </div>
                    <div class="col-sm-6 col-md-5 mr-1  mb-2">
                        <a href="#" type="button" class="btn btn-index  btn-lg form-control">Subscribe</a>
                    </div>

                </div>
            </div>
        </div>
        <div class="row copyright-section">
            <div class="col copyright mt-1">
                <p class=""><small class="text-white-50"><a href="#">Develop By TheKodeAlpha.com</a></small></p>
            </div>
            <div class="col copyright mt-1">
                <p class=""><small class="text-white-50">© <?php echo date("Y");?> All Rights Reserved.</small></p>
            </div>
        </div>
    </div>
</section>