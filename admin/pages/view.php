
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <h1>View</h1>
    <!-- basic form  -->
                        <!-- ============================================================== -->
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="section-block" id="basicform">
                                    <h3 class="section-title">Basic Form Elements</h3>
                                    <p>Use custom button styles for actions in forms, dialogs, and more with support for multiple sizes, states, and more.</p>
                                </div>
                                <div class="card">
                                    <h5 class="card-header">Basic Form</h5>
                                    <div class="card-body">
                                        <form>
                                            <div class="form-group">
                                                <label for="inputText3" class="col-form-label">ID</label>
                                                <input id="inputText3" name ="id" type="text" class="form-control" placeholder="ID">
                                            </div>
                                            <div class="form-group">
                                                <label for="inputText3" class="col-form-label">Product ID</label>
                                                <input id="inputText3" name ="product_id" type="text" class="form-control"placeholder="Product ID">
                                            </div>
                                            <div class="form-group">
                                                <label for="inputText4" class="col-form-label">Product Name</label>
                                                <input id="inputText4" name ="product_name" type="text" class="form-control" placeholder="Product Name">
                                            </div>
                                            <div class="custom-file mb-3">
                                                <input type="file" class="custom-file-input" id="customFile">
                                                <label class="custom-file-label" name ="product_image" for="customFile">Chose an Image</label>
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleFormControlTextarea1">Product Description</label>
                                                <textarea class="form-control" name = "product_description" id="exampleFormControlTextarea1" rows="3"></textarea>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- ============================================================== -->
                        <!-- end basic form  -->
        </div>
    </div>
</div>