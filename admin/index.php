
<?php
session_start();
	// $_SESSION['name'] = "$name";
    // header('location: ../admin/pages/login.php');
    if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
        $welcomeMessage = "Welcome to the Admin Panel Here, " . $_SESSION['name'] . "!";
    } else {
        header('Location: ../admin/pages/login.php');
    }

?> 

<?php include("header.php")?>

<?php include("menu.php")?>

 <!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <?php 
        if(isset($_GET['req']) && $_GET['req'] == "addProduct"){
            include 'pages/add-product.php';
        } elseif(isset($_GET['req']) && $_GET['req'] == "dashboard"){
            include 'pages/dashboard.php';
        }
        elseif(isset($_GET['req']) && $_GET['req']  == "viewProducts"){
            include 'pages/view.php';
        }
        ?>
        <?
    if(!empty($welcomeMessage)) echo $welcomeMessage;
?>
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
           <?php include("footer.php")?>